//
//  SignUpViewController.swift
//  InClassExercisesStarter
//
//  Created by admin on 11/27/18.
//  Copyright © 2018 room1. All rights reserved.
//

import UIKit
import FirebaseFirestore
import Firebase

class SignUpViewController: UIViewController{

    var db = Firestore.firestore()
    var photoSelected = ""
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var errorLabel: UILabel!

    @IBOutlet weak var ashbtn: UIButton!
    @IBAction func ashButton(_ sender: Any) {
        print("Ash selected")
        photoSelected = "ash"
        /*
        self.ashbtn.backgroundColor = .clear
        self.ashbtn.layer.cornerRadius = 20
        self.ashbtn.layer.borderWidth = 3
        self.ashbtn.layer.borderColor = UIColor.red.cgColor
        */
    }
    @IBOutlet weak var brockbtn: UIButton!
    @IBAction func brockButton(_ sender: Any) {
        print("Brock selected")
        photoSelected = "brock"
    }
    @IBOutlet weak var mistybtn: UIButton!
    @IBAction func mistyButton(_ sender: Any) {
        print("Misty selected")
        photoSelected = "misty"
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        }

    @IBAction func signUpButton(_ sender: Any) {
        print("pressed signup button")
        let email = emailTextField.text!
        let name = nameTextField.text!
        let password = passwordTextField.text!
        if email != nil || email != "" || name != nil || name != "" || password != nil || password != "" || photoSelected != ""{
            
            Auth.auth().createUser(withEmail: email, password: password) {
                
                (user, error) in
                
                if (user != nil) {
                    // New user created!
                    print("Created user: ")
                    print("User id: \(user?.user.uid)")
                    print("Email: \(user?.user.email)")
                    self.errorLabel.text = "User Created"
                    
                    let use = self.db.collection("users")
                    use.document(email).setData([
                        "name": email,
                        "latitude": 0.0,
                        "longitude": 0.0,
                        "pokemon": "",
                        "icon": "",
                        "status": "offline",
                        "money" : 200,
                        "pokemonsDefeated" : 0,
                        "photo" : self.photoSelected 
                        ])
                    
                    let vcontrol = self.storyboard?.instantiateViewController(withIdentifier: "VC")
                    self.navigationController?.pushViewController(vcontrol!, animated: true)
                }
                else {
                    print("ERROR!")
                    print(error?.localizedDescription)
                    self.errorLabel.text = error?.localizedDescription
                }
            }
        }else{
            self.errorLabel.text = "Fill all fields!"
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
