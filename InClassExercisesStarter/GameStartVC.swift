//
//  GameStartVC.swift
//  InClassExercisesStarter
//
//  Copyright © 2018 room1. All rights reserved.
//

import UIKit
import FirebaseFirestore
import Firebase

class GameStartVC: UIViewController {
    var userName = ""
    var playerlat = 0.0
    var playerlng = 0.0
     var playerid = ""
    var money = 0
    var pokemonDefeated = 0
    var healthPoints = 0
    var selectedPoke = ""
    var playerSelectedd = Player();
   
    @IBOutlet weak var user2img: UIImageView!
    @IBOutlet weak var user1img: UIImageView!
    var user1HP = 0
    var user1Level  = 0
    var user1Defense = 0
    
    var user2HP = 0
    var user2Level  = 0
    var user2Defense = 0
    var isTheWinner = false
    
    @IBOutlet weak var user2: UILabel!
    //var user1HP = 0
    //var user2HP = 0
    var level = 1;
    var count = 0
     var db:Firestore!
    var attempts = 0
    @IBOutlet weak var scoreP2: UILabel!
    
    @IBOutlet weak var scoreP1: UILabel!
    
    @IBAction func attack1(_ sender: Any) {
        
        if(user1Level < 1)
        {
            let message = "You can't use this attack."
            let infoAlert = UIAlertController(title: "Attack Failed!", message: message, preferredStyle: .alert)
            infoAlert.addAction(UIAlertAction(title: "Close", style: .default, handler: nil))
            self.present(infoAlert, animated: true, completion: nil)
        }
            
        else if (user2HP <= 0 )
        {
            isTheWinner = true
           // playAgain()
            winMsg()
            UserData()
        }
            
        else{
            
            // ====================  LEVEL 1 =================================
            if(user1Level > 0)
            {
                if(user1HP > 0 || user2HP > 0)
                {
                print("attack1")
                user1HP = user1HP+Int.random(in: 0 ..< 50)
                user2HP = user2HP-Int.random(in: 20 ..< 50)
                print("count : \(user1HP)")
                scoreP1.text = String(user1HP)
                scoreP2.text = String(user2HP)
               
                // player 2 turn
                
                user1HP = user1HP-Int.random(in: 20 ..< 80)
                user2HP = user2HP+Int.random(in: 0 ..< 10)
                scoreP2.text = String(user2HP)
                scoreP1.text = String(user1HP)
                    
                    
                    
                    if (user2HP <= 0 )
                    {
                        isTheWinner = true
                        //playAgain()
                        winMsg()
                        UserData()
                    }
                    
                    
                    if (user1HP <= 0 )
                    {
                        isTheWinner = false
                        //playAgain()
                        lostMsg()
                        UserData()
                    }
            }
                
               else  if(user1HP <= 0)
                {
                    isTheWinner = false
                    lostMsg()
                   // playAgain()
                    UserData()
                }
                    
                else if (user2HP <= 0)
                {
                    isTheWinner = true
                    winMsg()
                    user1Level = user1Level+1
                    print("u level \(user1Level)")
                    UserData()
                    //playAgain()

                }
                
                
                
            }
        }
    }

    @IBAction func attack2(_ sender: Any) {
        if(user1Level <= 1)
        {
            let message = "You are at level 1. you cant use this attack."
            let infoAlert = UIAlertController(title: "Attack Failed!", message: message, preferredStyle: .alert)
            infoAlert.addAction(UIAlertAction(title: "Close", style: .default, handler: nil))
            self.present(infoAlert, animated: true, completion: nil)
        }
            
        else{
                
            if(user1HP > 0 || user2HP > 0)
            {
                print("attack1")
                user1HP = user1HP+Int.random(in: 0 ..< 50)
                user2HP = user2HP-Int.random(in: 0 ..< 50)
                print("count : \(user1HP)")
                scoreP1.text = String(user1HP)
                scoreP2.text = String(user2HP)
                
                // player 2 turn
                
                user1HP = user1HP-Int.random(in: 0 ..< 50)
                user2HP = user2HP+Int.random(in: 0 ..< 50)
                scoreP2.text = String(user2HP)
                scoreP1.text = String(user1HP)
            }
                
            else  if(user1HP <= 0)
            {
                isTheWinner = false
                lostMsg()
                //playAgain()
                UserData()
            }
                
            else if (user2HP <= 0)
            {
                isTheWinner = true
                winMsg()
                user1Level = user1Level+1
                print("u level \(user1Level)")
                UserData()
               // playAgain()

            }
                
        }
        
        
    }
    
    @IBAction func attack3(_ sender: Any) {
        if(user1Level<=2)
        {
            let message = "Attack enable after level 2 "
            let infoAlert = UIAlertController(title: "Attack Failed!", message: message, preferredStyle: .alert)
            infoAlert.addAction(UIAlertAction(title: "Close", style: .default, handler: nil))
            self.present(infoAlert, animated: true, completion: nil)
        }
            
        else{
        
            if(user1HP > 0 || user2HP > 0)
            {
                print("attack1")
                user1HP = user1HP+Int.random(in: 0 ..< 50)
                user2HP = user2HP-Int.random(in: 0 ..< 50)
                print("count : \(user1HP)")
                scoreP1.text = String(user1HP)
                scoreP2.text = String(user2HP)
                
                // player 2 turn
                
                user1HP = user1HP-Int.random(in: 0 ..< 50)
                user2HP = user2HP+Int.random(in: 0 ..< 50)
                scoreP2.text = String(user2HP)
                scoreP1.text = String(user1HP)
            }
                
            else  if(user1HP <= 0)
            {
                isTheWinner = false
                lostMsg()
               // playAgain()
                UserData()
            }
            else if (user2HP <= 0)
            {
                isTheWinner = true
                winMsg()
                user1Level = user1Level+1
                UserData()
             //   playAgain()

            }
                
            }
        
        
    }
    
    @IBAction func attack4(_ sender: Any) {
        if(user1Level<3)
        {
            let message = "Attack enable after level 3 "
            let infoAlert = UIAlertController(title: "Attack Failed!", message: message, preferredStyle: .alert)
            infoAlert.addAction(UIAlertAction(title: "Close", style: .default, handler: nil))
            self.present(infoAlert, animated: true, completion: nil)
        }
            
        else{
            
            if(user1HP > 0 || user2HP > 0)
            {
                print("attack1")
                user1HP = user1HP+Int.random(in: 0 ..< 50)
                user2HP = user2HP-Int.random(in: 0 ..< 50)
                print("count : \(user1HP)")
                scoreP1.text = String(user1HP)
                scoreP2.text = String(user2HP)
                
                // player 2 turn
                
                user1HP = user1HP-Int.random(in: 0 ..< 50)
                user2HP = user2HP+Int.random(in: 0 ..< 50)
                scoreP2.text = String(user2HP)
                scoreP1.text = String(user1HP)
            }
                
            else  if(user1HP <= 0)
            {
                isTheWinner = false
                lostMsg()
                //playAgain()
                UserData()
            }
            else if (user2HP <= 0)
            {
                isTheWinner = true
                winMsg()
                user1Level = user1Level+1
                UserData()
                //playAgain()

            }
            
        }
        
        
    }
    
    func  UserData() {
        
        print("u level ud \(user1Level)")
        let user = db.collection("users").whereField("name", isEqualTo: self.userName)
        user.getDocuments() {
            (querySnapshot, err) in
            if (querySnapshot?.count ?? 0 > 0) {
                print("old:\( querySnapshot?.count)")
                
                for document in querySnapshot!.documents {
                    //if(document.documentID == self.username){
                    print("oldoldold old old old old1111111111111111")
                    let use = self.db.collection("users")
                   // var n = self.username
                    use.document(self.userName).setData([
                        "name": document["name"],
                        "latitude": document["latitude"],
                        "longitude": document["longitude"],
                        "pokemon": document["pokemon"],
                        "icon": document["icon"],
                        "status": "online",
                        "money" : document["money"]! as! Int,
                        "pokemonsDefeated" : document["pokemonsDefeated"]! as! Int+1,
                        ])
                }
            }
        }
        
        
        let pok = db.collection("userPokemon").whereField("name", isEqualTo: selectedPoke)
        pok.getDocuments() {
            (querySnapshot, err) in
            if (querySnapshot?.count ?? 0 > 0) {
                print("old:\( querySnapshot?.count)")
                
                for document in querySnapshot!.documents {
                    //if(document.documentID == self.username){
                    print("oldoldold old old old old1111111111111111")
                    let use = self.db.collection("Pokemon")
                    // var n = self.username
                    use.document(self.userName).setData([
                        "name": self.selectedPoke,
                        "Health Point": self.user1HP,
                        "defence": document["defence"],
                        "action": document["action"],
                        "level": self.user1Level,
                        "currentHealth": document["currentHealth"],
                        "id": self.userName
                        ])
                }
            }
        }
        
        
    }
    
    func winMsg()
    {
        let message = "You won. You reached to next level"
        let infoAlert = UIAlertController(title: "You reached to new level 👍", message: message, preferredStyle: .alert)
        infoAlert.addAction(UIAlertAction(title: "Close", style: .default, handler: nil))
        self.present(infoAlert, animated: true, completion: nil)
        user1HP = user1HP + Int.random(in: 100 ..< 300)
    }
    
    func lostMsg()
    {
        let message = "You lose"
        let infoAlert = UIAlertController(title: "Lose", message: message, preferredStyle: .alert)
        infoAlert.addAction(UIAlertAction(title: "Close", style: .default, handler: nil))
        self.present(infoAlert, animated: true, completion: nil)
    }
    
    func playAgain()
    {
        gameHasFinished(enemylvl: user2Level, playerWinned : isTheWinner)
        let message = "Go back and Play Again"
        let infoAlert = UIAlertController(title: "Play again", message: message, preferredStyle: .alert)
        infoAlert.addAction(UIAlertAction(title: "Close", style: .default, handler: nil))
        self.present(infoAlert, animated: true, completion: nil)
        
    }
    
    
    @IBOutlet weak var usernameLbl: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        db = Firestore.firestore()
        let addbutton = UIBarButtonItem(title: "Score Board", style: .done, target: self, action: #selector(ScoreButton))
        self.navigationItem.rightBarButtonItem = addbutton
       
        let uusername = UserDefaults.standard.string(forKey: "username") ?? ""
        
        usernameLbl.text = uusername
        
        print("user : \(userName)")
        //=========================================================================
        // get user data player 1
        //==========================================================================
        let pl1 = db.collection("users").whereField("name", isEqualTo: userName)
        pl1.getDocuments() {
            (querySnapshot, err) in
            if (querySnapshot?.count ?? 0 > 0){
                for document in querySnapshot!.documents {
                    print("\(document.documentID) => \(document.data())")
                    // self.pokemonData[document.documentID] =  document.data()
                    self.playerid = document["name"]! as! String
                    print(self.playerid)
                    //self.user2.text = self.playerid
                    self.money = document["money"]! as! Int
                    print("money user 1  = \(self.money)")
                    
                    self.pokemonDefeated = document["pokemonsDefeated"]! as! Int
                    print("defeated  user 1= \(self.pokemonDefeated)")
                    
                    self.selectedPoke = document["pokemon"]! as! String
                    print("selected user 1 = \(self.selectedPoke)")
                    
                    var img = document["icon"] as! String
                    img = img.replacingOccurrences(of: ".png", with: "")
                    var myimg: UIImage = UIImage(named: img)!
                    self.user1img.image = myimg
                    
                    // get pokemon details
                    let pokemonP = self.db.collection("Pokemon").whereField("name", isEqualTo: self.selectedPoke)
                    pokemonP.getDocuments() {
                        (querySnapshot, err) in
                        if (querySnapshot?.count ?? 0  >= 0){
                            for document in querySnapshot!.documents {
                                print("\(document.documentID) => \(document.data())")
                                print(document["action"]!)
                                self.user1HP = document["Health Point"] as! Int
                                 self.user1Defense = document["defence"] as! Int
                                 self.user1Level = document["level"] as! Int
                                self.scoreP1.text = String(self.user1HP)
                             
                                print("level 1 \(self.user1Level)")
                                print("level 1 def \(self.user1Defense)")
                                print("level 1 hp \(self.user1HP)")

                                
                }
                }
            }
             
                }}
            
            else
            {
                self.playerid = "default.com"
                print(self.playerid)
                self.user2.text = self.playerid
            }
        }
        
        
        
        
        
        // ==================== player 1 end ==================
        
        
        
        
        // get player 2 data
            let pl2 = self.db.collection("users").whereField("latitude", isEqualTo: self.playerlat)
        pl2.getDocuments() {
            (querySnapshot, err) in
            if (querySnapshot?.count ?? 0 > 0){
                for document in querySnapshot!.documents {
                    print("\(document.documentID) => \(document.data())")
                    // self.pokemonData[document.documentID] =  document.data()
                    self.playerid = document["name"]! as! String
                    print(self.playerid)
                    self.user2.text = self.playerid
                    var img = document["icon"] as! String
                    img = img.replacingOccurrences(of: ".png", with: "")
                    var myimg: UIImage = UIImage(named: img)!
                    self.user2img.image = myimg
                    
                    // get pokemon details
                    let pokemonP2 = self.db.collection("Pokemon").whereField("name", isEqualTo: document["pokemon"]! )
                    pokemonP2.getDocuments() {
                        (querySnapshot, err) in
                        if (querySnapshot?.count ?? 0  >= 0){
                            for document in querySnapshot!.documents {
                                print("\(document.documentID) => \(document.data())")
                                print(document["action"]!)
                                self.user2HP = document["Health Point"] as! Int
                                self.user2Defense = document["defence"] as! Int
                                self.user2Level = document["level"] as! Int
                                self.scoreP2.text = String(self.user2HP)
                                
                                
                                print("level 1 2 \(self.user2Level)")
                                print("level 2 def \(self.user2Defense)")
                                print("level 2 hp \(self.user2HP)")
                                
                                
                            }
                        }
                    }
                    //self.money = document["money"]! as! Int
                   // print("money = \(self.money)")
                    
//                    self.pokemonDefeated = document["pokemonsDefeated"]! as! Int
//                    print("defeated = \(self.pokemonDefeated)")
//
//                    self.selectedPoke = document["pokemon"]! as! String
//                    print("selected = \(self.selectedPoke)")
                    
                   
                    }
            }else{
                self.playerid = "default.com"
                print(self.playerid)
                self.user2.text = self.playerid
            }

            let pl2pokemon = self.db.collection("userPokemon").whereField("id", isEqualTo: self.playerid)
        pl2pokemon.getDocuments() {
            (querySnapshot, err) in
            if (querySnapshot?.count ?? 0  >= 0){
                for document in querySnapshot!.documents {
                    print("\(document.documentID) => \(document.data())")
                    // self.pokemonData[document.documentID] =  document.data()
                    
                    
                    
                }
            }else{
                self.playerid = "default.com"
                let pl2pokemon = self.db.collection("userPokemon").whereField("id", isEqualTo: "default.com")
                    pl2pokemon.getDocuments() {
                    (querySnapshot, err) in
                    for document in querySnapshot!.documents {
                        print("\(document.documentID) => \(document.data())")
                        // self.pokemonData[document.documentID] =  document.data()
                    }
                }
            }
            
        }
        
    }
    }
    
    @objc func ScoreButton() {
        let vcontrol = storyboard?.instantiateViewController(withIdentifier: "SVC")
        self.navigationController?.pushViewController(vcontrol!, animated: true)
        
    }
    
    func gameHasFinished(enemylvl: Int, playerWinned : Bool){
//
//        print("game finish")
//        print(playerSelectedd.Player_email!)
//        if playerWinned {
//            //exp to pokemon
//            let expToAdd = 100*enemylvl;
//
//            //raising lvl if the exp caps
//            if ((playerSelectedd.Player_pokemon?.Pokemon_exp!)! + expToAdd > 1000){
//                if playerSelectedd.Player_pokemon?.Pokemon_level! != 4{
//                    //ad exp and lvl
//                    let expToNextLvl = 1000 - (playerSelectedd.Player_pokemon?.Pokemon_exp!)!;
//                    playerSelectedd.Player_pokemon?.Pokemon_exp! = expToAdd - expToNextLvl
//                    playerSelectedd.Player_pokemon?.Pokemon_level = (playerSelectedd.Player_pokemon?.Pokemon_level!)! + 1;
//                    //increase pokemon stats
//                    //hp
//                    playerSelectedd.Player_pokemon?.Pokemon_hp = getNewStats(lvl: (playerSelectedd.Player_pokemon?.Pokemon_level!)!, value: (playerSelectedd.Player_pokemon?.Pokemon_hp!)!)
//                    //attack
//                    playerSelectedd.Player_pokemon?.Pokemon_attack = getNewStats(lvl: (playerSelectedd.Player_pokemon?.Pokemon_level!)!, value: (playerSelectedd.Player_pokemon?.Pokemon_attack!)!)
//                    //defence
//                    playerSelectedd.Player_pokemon?.Pokemon_defence = getNewStats(lvl: (playerSelectedd.Player_pokemon?.Pokemon_level!)!, value: (playerSelectedd.Player_pokemon?.Pokemon_defence!)!)
//
//
//                    //TODO: add new move
//                }
//            }else{
//                //add exp
//                playerSelectedd.Player_pokemon?.Pokemon_exp! = (playerSelectedd.Player_pokemon?.Pokemon_exp!)! + expToAdd;
//            }
//
//
//
//            //add money to the player
//            let moneyToAdd = 50*enemylvl;
//            playerSelectedd.Player_money! = playerSelectedd.Player_money! + moneyToAdd;
//
//
//            //the current pokemon hp will be updated every time that he is hit
//        }else{
//            //subtract player's money
//            let moneyToRemove = Int(Double((playerSelectedd.Player_pokemon!.Pokemon_exp!))*0.6);
//            playerSelectedd.Player_money! = playerSelectedd.Player_money! + moneyToRemove;
//        }
//
//        //update on firebase
//        let pokemonS = db.collection("userPokemon").document(playerSelectedd.Player_email!)
//        pokemonS.updateData([
//            "Health Point" : playerSelectedd.Player_pokemon!.Pokemon_hp!,
//            "action" : playerSelectedd.Player_pokemon!.Pokemon_attack!,
//            "currentHealth" : playerSelectedd.Player_pokemon!.Pokemon_current_hp!,
//            "defence" : playerSelectedd.Player_pokemon!.Pokemon_defence!,
//            "exp" : playerSelectedd.Player_pokemon!.Pokemon_exp!,
//            "level" : playerSelectedd.Player_pokemon!.Pokemon_level!
//        ]) { err in
//            if let err = err {
//                print("Error updating document: \(err)")
//            } else {
//                print("Pokemon Stats updated")
//            }
//        }
//
//        let userS = db.collection("users").document(playerSelectedd.Player_email!)
//        var pokemonsDefeated = 0
//        userS.getDocument { (document, error) in
//            if let document = document, document.exists {
//                let dataDescription = document.data()
//                pokemonsDefeated = dataDescription!["pokemonsDefeated"] as! Int
//            } else {
//                print("Document does not exist")
//            }
//        }
//
//        userS.updateData([
//            "money" : playerSelectedd.Player_money!,
//            "pokemonsDefeated" : (pokemonsDefeated + 1)
//
//        ]) { err in
//            if let err = err {
//                print("Error updating document: \(err)")
//            } else {
//                print("Player Money updated")
//            }
//        }

    }

    func getNewStats(lvl: Int, value : Int) -> Int {
        if( lvl == 1){
            return value;
        }else{
            return (value / lvl) + getNewStats(lvl: (lvl-1), value: value);
        }
    }


    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.

        let n1 = segue.destination as! ScoreViewController
        n1.username = self.userName
    }



    
}
