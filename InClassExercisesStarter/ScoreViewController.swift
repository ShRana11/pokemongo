//
//  ScoreViewController.swift
//  InClassExercisesStarter
//
//  Created by Sukhwinder Rana on 2018-12-13.
//  Copyright © 2018 room1. All rights reserved.
//

import UIKit
import FirebaseFirestore
import  Firebase

class ScoreViewController: UIViewController {
    @IBOutlet weak var resultImage1: UIImageView!
    var username = ""
    var keys:[String] = [String]()
    var value:[String] = [String]()
    var image:[String] = [String]()
     var db:Firestore!
     var userdata:[String:[String:Any]] = [:]
    
    @IBOutlet weak var resultImage4: UIImageView!
    @IBOutlet weak var resultImage3: UIImageView!
    @IBOutlet weak var resultImage2: UIImageView!
    @IBOutlet weak var resultLabel5: UILabel!
    @IBOutlet weak var resultLabel4: UILabel!
    @IBOutlet weak var resultLabel3: UILabel!
    @IBOutlet weak var resultLabel2: UILabel!
    
    @IBOutlet weak var resultLabel1: UILabel!
    

    override func viewDidLoad() {
        super.viewDidLoad()
         db = Firestore.firestore()
        var s = 0
        db.collection("users").order(by: "pokemonsDefeated", descending: true).limit(to: 4).getDocuments() {
            (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                for document in querySnapshot!.documents {
                    //print("\(document.documentID) = \(document.data())")
                    self.userdata[document.documentID] =  document.data()
                    print(self.userdata[document.documentID] ?? "unknown")
                }
               for i in self.userdata.keys {
                //var i = self.userdata.values
                self.keys.append(i)
               
                   // self.resultLabel1.text = "\(i["name"]! as! String)   \(i["pokemonsDefeated"]! as! Int)"
            
                }
                for i in self.userdata.values{
                    //var i = self.userdata.values
                    self.value.append("\(i["pokemonsDefeated"]! as! Int)")
                    self.image.append("\(i["icon"]! as! String)")
                    // self.resultLabel1.text = "\(i["name"]! as! String)   \(i["pokemonsDefeated"]! as! Int)"
                    
                }
                
           //  self.pokemonImage.image = UIImage(named: self.rowImage)
                
                self.resultLabel1.text = "  \(self.keys[0] as! String)               \(self.value[0] as! String)"
                self.resultImage1.image = UIImage(named: self.image[0] as! String)
                self.resultLabel2.text = "  \(self.keys[1] as! String)               \(self.value[1] as! String)"
                 self.resultImage2.image = UIImage(named: self.image[1] as! String)
                self.resultLabel3.text = "  \(self.keys[2] as! String)               \(self.value[2] as! String)"
                self.resultImage3.image = UIImage(named: self.image[2] as! String)
                self.resultLabel4.text = "  \(self.keys[3] as! String)               \(self.value[3] as! String)"
                self.resultImage4.image = UIImage(named: self.image[3] as! String)
            }
        }

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
